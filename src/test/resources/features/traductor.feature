# language: es
# Autor: Jhon Escudero
@stories
Feature:
    Como un suario
    Quiero ingresar a google Translator
    A traducir palabras entre diferentes lenguajes
  @scenario1
  Scenario: Traducir de Ingeles a Español
    Given que Yeison quiere usar el traductor de google
    When el traduce una palabra de inglés a español
      |origen     |destino    |palabrai   |
      |inglés     |español    |table      |
    Then el deberia ver la palabra traducida del idioma origen a idioma destino
      |palabrae   |
      |mesa       |